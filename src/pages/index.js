import React from 'react'

import Layout from '../components/layout'

const IndexPage = () => (
  <Layout>
    <h1>Under Construction</h1>
    <p>Check back soon for a new experience.</p>
  </Layout>
)

export default IndexPage
